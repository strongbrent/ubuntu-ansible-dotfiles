# Ubuntu Ansible Dotfiles
Configuration management solution used to automate the installation/customization of a Ubuntu 20.04 SRE virtual machine environment.

## Requirements

To configure your host computer and provision the virtual machine, you will need to install a small number of required applications:

- [git](https://git-scm.com/)  
- [VirtualBox](https://www.virtualbox.org/)  
- [Vagrant](https://www.vagrantup.com/)  

## Quickstart 

To kick everything off, clone this repo and then navigate to the repo directory.  

To create the VM, run the following:  
```
vagrant up
```  

To log into the VM via SSH:
```
vagrant ssh
```  

To rerun the Ansible playbooks:
```
vagrant provision
```  

And, to delete the VM:
```
vagrant destroy
```
