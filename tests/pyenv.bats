#!/usr/bin/env bats
# File: tests/pyenv.bats

# --- PYENV Role Tests --------------------------------------------------
@test "Checking for directory: ~/.pyenv" {
    run test -d ${HOME}/.pyenv
    [ "${status}" -eq 0 ]
}

@test "Checking for command: pyenv" {
    run command -v pyenv
    [ "${status}" -eq 0 ]
}

@test "Checking for command verion: pyenv" {
    run pyenv --version
    [ "${status}" -eq 0 ]
}