#!/usr/bin/env bash

files=(
    aws
    azure
    common
    docker
    doctl
    gcloud
    goenv
    hashistack
    heroku
    kubernetes
    k3d
    oh-my-zsh
    nvm
    onepassword
    pyenv
    poetry
    rust
    vimrc
)

for file in "${files[@]}"
do
    echo "Executing test file: ${file}.bats"
    bats ./${file}.bats
done
