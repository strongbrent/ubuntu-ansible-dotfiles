#!/usr/bin/env bats
# File: tests/hashistack.bats

# --- Hashistack Role Tests --------------------------------------------------
@test "Checking for command: packer" {
    run command -v packer
    [ "${status}" -eq 0 ]
}

@test "Checking for packer version: v1.7.2" {
    run packer version
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Packer v1.7.2" ]
}

@test "Checking for command: terraform" {
    run command -v terraform
    [ "${status}" -eq 0 ]
}

@test "Checking for terraform version: v0.14.3" {
    run terraform version
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Terraform v0.14.3" ]
}
