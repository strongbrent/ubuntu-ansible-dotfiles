#!/usr/bin/env bats
# File: tests/k3d.bats

# --- k3d Role Tests --------------------------------------------------
@test "Checking for command: k3d" {
    run command -v k3d
    [ "${status}" -eq 0 ]
}

