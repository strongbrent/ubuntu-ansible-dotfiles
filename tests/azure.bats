#!/usr/bin/env bats
# File: tests/azure.bats

# --- Azure Role Tests --------------------------------------------------
@test "Checking for command: azure-cli" {
    run command -v az
    [ "${status}" -eq 0 ]
}