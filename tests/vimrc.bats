#!/usr/bin/env bats
# File: tests/vimrc.bats

# --- VIMRC Role Tests --------------------------------------------------
@test "Checking for directory: ~/.vim_runtime" {
    run test -d ${HOME}/.vim_runtime
    [ "${status}" -eq 0 ]
}

@test "Checking feature is disabled: section folding" {
    run grep "^set nofoldenable" ${HOME}/.vim_runtime/my_configs.vim
    [ "${status}" -eq 0 ]
}

@test "Checking for alias in .zshrc: vimrc_update" {
    run grep "^alias vimrc_update=" ${HOME}/.zshrc
    [ "${status}" -eq 0 ]
}
