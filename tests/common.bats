#!/usr/bin/env bats
# File: tests/common.bats

# --- Common Role Tests --------------------------------------------------
@test "Checking for package: ansible" {
    run dpkg -s ansible
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: ansible" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: apt-file" {
    run dpkg -s apt-file
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: apt-file" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: apt-transport-https" {
    run dpkg -s apt-transport-https
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: apt-transport-https" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: aptitude" {
    run dpkg -s aptitude
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: aptitude" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: arpwatch" {
    run dpkg -s arpwatch
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: arpwatch" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: atop" {
    run dpkg -s atop
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: atop" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: bats" {
    run dpkg -s bats
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: bats" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: bmon" {
    run dpkg -s bmon
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: bmon" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: build-essential" {
    run dpkg -s build-essential
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: build-essential" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: bzip2" {
    run dpkg -s bzip2
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: bzip2" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: ca-certificates" {
    run dpkg -s ca-certificates
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: ca-certificates" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: chrony" {
    run dpkg -s chrony
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: chrony" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: clang" {
    run dpkg -s clang
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: clang" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: clang-7" {
    run dpkg -s clang-7
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: clang-7" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: curl" {
    run dpkg -s curl
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: curl" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: default-jdk" {
    run dpkg -s default-jdk
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: default-jdk" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: direnv" {
    run dpkg -s direnv
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: direnv" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: dnsutils" {
    run dpkg -s dnsutils
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: dnsutils" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: dos2unix" {
    run dpkg -s dos2unix
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: dos2unix" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: dstat" {
    run dpkg -s dstat
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: dstat" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: ethtool" {
    run dpkg -s ethtool
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: ethtool" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: flang-7" {
    run dpkg -s flang-7
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: flang-7" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: fort77" {
    run dpkg -s fort77
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: fort77" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: gfortran" {
    run dpkg -s gfortran
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: gfortran" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: glances" {
    run dpkg -s glances
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: glances" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: gnupg-agent" {
    run dpkg -s gnupg-agent
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: gnupg-agent" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: gnupg2" {
    run dpkg -s gnupg2
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: gnupg2" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: hardinfo" {
    run dpkg -s hardinfo
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: hardinfo" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: htop" {
    run dpkg -s htop
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: htop" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: httpie" {
    run dpkg -s httpie
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: httpie" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: iftop" {
    run dpkg -s iftop
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: iftop" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: iotop" {
    run dpkg -s iotop
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: iotop" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: iperf3" {
    run dpkg -s iperf3
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: iperf3" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: iptraf" {
    run dpkg -s iptraf
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: iptraf" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: jq" {
    run dpkg -s jq
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: jq" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: libncurses5-dev" {
    run dpkg -s libncurses5-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: libncurses5-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: libncursesw5-dev" {
    run dpkg -s libncursesw5-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: libncursesw5-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: libffi-dev" {
    run dpkg -s libffi-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: libffi-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: libbz2-dev" {
    run dpkg -s libbz2-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: libbz2-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: liblzma-dev" {
    run dpkg -s liblzma-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: liblzma-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: libreadline-dev" {
    run dpkg -s libreadline-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: libreadline-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: libsqlite3-dev" {
    run dpkg -s libsqlite3-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: libsqlite3-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: libssl-dev" {
    run dpkg -s libssl-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: libssl-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: llvm" {
    run dpkg -s llvm
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: llvm" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: lmod" {
    run dpkg -s lmod
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: lmod" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: make" {
    run dpkg -s make
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: make" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: mtr" {
    run dpkg -s mtr
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: mtr" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: multitail" {
    run dpkg -s multitail
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: multitail" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: mysql-client" {
    run dpkg -s mysql-client
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: mysql-client" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: ncat" {
    run dpkg -s ncat
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: ncat" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: ncdu" {
    run dpkg -s ncdu
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: ncdu" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: net-tools" {
    run dpkg -s net-tools
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: net-tools" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: netcat" {
    run dpkg -s netcat
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: netcat" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: nethogs" {
    run dpkg -s nethogs
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: nethogs" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: ngrep" {
    run dpkg -s ngrep
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: ngrep" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: nmap" {
    run dpkg -s nmap
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: nmap" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: openssh-server" {
    run dpkg -s openssh-server
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: openssh-server" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: openvpn" {
    run dpkg -s openvpn
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: openvpn" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: p7zip-full" {
    run dpkg -s p7zip-full
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: p7zip-full" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: parallel" {
    run dpkg -s parallel
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: parallel" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: pipenv" {
    run dpkg -s pipenv
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: pipenv" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: postgresql-client" {
    run dpkg -s postgresql-client
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: postgresql-client" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: pwgen" {
    run dpkg -s pwgen
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: pwgen" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: python-openssl" {
    run dpkg -s python-openssl
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: python-openssl" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: python3-pip" {
    run dpkg -s python3-pip
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: python3-pip" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: python3-pandas" {
    run dpkg -s python3-pandas
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: python3-pandas" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: python3-venv" {
    run dpkg -s python3-venv
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: python3-venv" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: ruby-full" {
    run dpkg -s ruby-full
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: ruby-full" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: screen" {
    run dpkg -s screen
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: screen" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: shellcheck" {
    run dpkg -s shellcheck
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: shellcheck" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: sipcalc" {
    run dpkg -s sipcalc
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: sipcalc" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: socat" {
    run dpkg -s socat
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: socat" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: software-properties-common" {
    run dpkg -s software-properties-common
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: software-properties-common" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: sqlite3" {
    run dpkg -s sqlite3
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: sqlite3" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: stress" {
    run dpkg -s stress
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: stress" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: sysstat" {
    run dpkg -s sysstat
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: sysstat" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: tcpdump" {
    run dpkg -s tcpdump
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: tcpdump" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: tk-dev" {
    run dpkg -s tk-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: tk-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: tmux" {
    run dpkg -s tmux
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: tmux" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: tree" {
    run dpkg -s tree
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: tree" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: unzip" {
    run dpkg -s unzip
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: unzip" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: vim" {
    run dpkg -s vim
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: vim" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: wget" {
    run dpkg -s wget
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: wget" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: whois" {
    run dpkg -s whois
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: whois" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: xz-utils" {
    run dpkg -s xz-utils
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: xz-utils" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for package: zlib1g-dev" {
    run dpkg -s zlib1g-dev
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Package: zlib1g-dev" ]
    [ "${lines[1]}" = "Status: install ok installed" ]
}

@test "Checking for timezone: America/Vancouver" {
    run cat /etc/timezone
    [ "${status}" -eq 0  ]
    [ "${lines[0]}" = "America/Vancouver"  ]
}

