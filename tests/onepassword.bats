#!/usr/bin/env bats
# File: tests/onepassword.bats

# --- 1Password Role Tests -----------------------------------------------
@test "Checking for command: op" {
    run command -v op
    [ "${status}" -eq 0 ]
}