#!/usr/bin/env bats
# File: tests/doctl.bats

# --- doctl Role Tests --------------------------------------------------
@test "Checking for command: doctl" {
    run command -v doctl
    [ "${status}" -eq 0 ]
}
