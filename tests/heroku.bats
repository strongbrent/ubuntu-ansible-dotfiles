#!/usr/bin/env bats
# File: tests/heroku.bats

# --- Heroku Role Tests --------------------------------------------------
@test "Checking for apt repo file: heroku.list" {
    run test -f /etc/apt/sources.list.d/heroku.list
    [ "${status}" -eq 0  ]
}

@test "Checking for package: heroku" {
    run dpkg -s heroku
    [ "${status}" -eq 0  ]
    [ "${lines[0]}" = "Package: heroku"  ]
    [ "${lines[1]}" = "Status: install ok installed"  ]                
}

