#!/usr/bin/env bats
# File: tests/oh-my-zsh.bats

# --- OH-MY-ZSH Role Tests --------------------------------------------------
@test "Checking for directory: ~/.oh-my-zsh" {
    run test -d ${HOME}/.oh-my-zsh
    [ "${status}" -eq 0 ]
}

@test "Checking for file: ~/.zshrc" {
    run test -f ${HOME}/.zshrc
    [ "${status}" -eq 0 ]
}

@test "Checking for custom plugin: zsh-autosuggestions" {
    run test -d ${HOME}/.oh-my-zsh/custom/plugins/zsh-autosuggestions
    [ "${status}" -eq 0 ]
}

@test "Checking for custom plugin: zsh-syntax-highlighting" {
    run test -d ${HOME}/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
    [ "${status}" -eq 0 ]
}

@test "Checking for default zsh theme: bira" {
    run grep ZSH_THEME=\"bira\" ~/.zshrc
    [ "${status}" -eq 0 ]
}

@test "Checking for zsh plugin list: git zsh-syntax-highlighting zsh-autosuggestions" {
    run grep "plugins=(git zsh-syntax-highlighting zsh-autosuggestions)" ~/.zshrc
    [ "${status}" -eq 0 ]
}

@test "Checking for zshrc alias: update" {
    run grep "^alias update=" ~/.zshrc
    [ "${status}" -eq 0 ]
}

@test "Checking for zshrc alias: autoremove" {
    run grep "^alias autoremove=" ~/.zshrc
    [ "${status}" -eq 0 ]
}

@test "Checking for zshrc alias: running_services" {
    run grep "^alias running_services=" ~/.zshrc
    [ "${status}" -eq 0 ]
}

@test "Checking for zshrc alias: get_ip" {
    run grep "^alias get_ip=" ~/.zshrc
    [ "${status}" -eq 0 ]
}

@test "Checking for zshrc alias: hgrep" {
    run grep "^alias hgrep=" ~/.zshrc
    [ "${status}" -eq 0 ]
}

@test "Checking for zshrc alias: cls" {
    run grep "^alias cls=" ~/.zshrc
    [ "${status}" -eq 0 ]
}

@test "Checking for default shell: zsh" {
    run echo ${SHELL}
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "/usr/bin/zsh" ]
}
