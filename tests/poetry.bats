#!/usr/bin/env bats
# File: tests/poetry.bats

# --- POETRY Role Tests --------------------------------------------------
@test "Checking for command: poetry" {
    run command -v poetry
    [ "${status}" -eq 0 ]
}

@test "Checking for command verion: poetry" {
    run poetry --version
    [ "${status}" -eq 0 ]
}
