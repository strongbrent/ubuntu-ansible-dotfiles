#!/usr/bin/env bats
# File: tests/docker.bats

# --- Docker Role Tests --------------------------------------------------
@test "Checking for command: docker" {
    run command -v docker
    [ "${status}" -eq 0 ]
}

@test "Checking for command: docker-compose" {
    run command -v docker-compose
    [ "${status}" -eq 0 ]
}

@test "Checking for user in docker group: vagrant" {
    run bash -c "grep docker /etc/group | grep vagrant"
    [ "${status}" -eq 0 ]
}