#!/usr/bin/env bats
# File: tests/goenv.bats

# --- GOENV Role Tests --------------------------------------------------
@test "Checking for directory: ~/.goenv" {
    run test -d ${HOME}/.goenv
    [ "${status}" -eq 0 ]
}

@test "Checking for command: goenv" {
    run command -v goenv
    [ "${status}" -eq 0 ]
}

@test "Checking for command verion: goenv" {
    run goenv --version
    [ "${status}" -eq 0 ]
}