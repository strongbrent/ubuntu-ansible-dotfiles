#!/usr/bin/env bats
# File: tests/gcloud.bats

# --- Google Cloud SDK Role Tests --------------------------------------------------
@test "Checking for command: google-cloud-sdk" {
    run command -v gcloud
    [ "${status}" -eq 0 ]
}