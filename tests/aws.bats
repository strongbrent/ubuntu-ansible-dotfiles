#!/usr/bin/env bats
# File: tests/aws.bats

# --- AWS Role Tests --------------------------------------------------
@test "Checking for command: awscli2" {
    run command -v aws
    [ "${status}" -eq 0 ]
}

@test "Checking for command: eksctl" {
    run command -v eksctl
    [ "${status}" -eq 0 ]
}

@test "Checking for command: awsume" {
    run command -v awsume
    [ "${status}" -eq 0 ]
}

@test "Checking for command: aws-parallelcluster" {
    run command -v pcluster
    [ "${status}" -eq 0 ]
}