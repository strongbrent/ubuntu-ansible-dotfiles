#!/usr/bin/env bats
# File: tests/nvm.bats

# --- NVM Role Tests --------------------------------------------------
@test "Checking for directory: ~/.nvm" {
    run test -d ${HOME}/.nvm
    [ "${status}" -eq 0 ]
}

@test "Checking for command: nvm.sh" {
    run test -f ${HOME}/.nvm/nvm.sh
    [ "${status}" -eq 0 ]
}

@test "Checking for NVM_DIR in .zshrc" {
    run grep "^export NVM_DIR=" ${HOME}/.zshrc
    [ "${status}" -eq 0 ]
}

