#!/usr/bin/env bats
# File: tests/rust.bats

# --- Rust Role Tests --------------------------------------------------
@test "Checking for command: rustc" {
    run command -v rustc
    [ "${status}" -eq 0 ]
}

@test "Checking for command: cargo" {
    run command -v cargo
    [ "${status}" -eq 0 ]
}

@test "Checking for command: rustup" {
    run command -v rustup
    [ "${status}" -eq 0 ]
}

