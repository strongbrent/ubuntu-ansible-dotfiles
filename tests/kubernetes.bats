#!/usr/bin/env bats
# File: tests/kubernetes.bats

# --- Kubernetes Role Tests --------------------------------------------------
@test "Checking for command: kubectl" {
    run command -v kubectl
    [ "${status}" -eq 0 ]
}

@test "Checking for kubectl version: 1.20.2-00" {
    run bash -c "dpkg -s kubectl | grep Version"
    [ "${status}" -eq 0 ]
    [ "${lines[0]}" = "Version: 1.20.2-00"  ]
}


@test "Checking for command: kubectx" {
    run command -v kubectx
    [ "${status}" -eq 0 ]
}

@test "Checking for command: kubens" {
    run command -v kubens
    [ "${status}" -eq 0 ]
}

@test "Checking for command: kompose" {
    run command -v kompose
    [ "${status}" -eq 0 ]
}

@test "Checking for command: helm" {
    run command -v helm
    [ "${status}" -eq 0 ]
}
