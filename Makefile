.PHONY: vm provision reload status

vm:
	vagrant up
provision:
	vagrant provision
reload:
	vagrant reload
status:
	vagrant status
