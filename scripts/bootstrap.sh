#!/usr/bin/env bash

source "/vagrant/scripts/lib/functions.sh"

# --- Global Script Variables --------------------------------------------
ansible_hosts_file="/etc/ansible/hosts"


# --- Helper Functions ---------------------------------------------------
# DESC: Creates ansible inventory file for this project
# ARGS: NONE
# OUT:  NONE
create_ansible_inventory () {
	if found_file "${ansible_hosts_file}"; then
		echo_task "Removing existing ansible hosts file: ${ansible_hosts_file}"
		sudo rm -fv "${ansible_hosts_file}"
	fi
	echo_task "Creating ansible inventory file: ${ansible_hosts_file}"
	echo "localhost              ansible_connection=local" | sudo tee "${ansible_hosts_file}"
}

# DESC: Installs additional ansible roles
# ARGS: NONE
# OUT:  NONE
install_ansible_roles() {
    local -r galaxy_cmd="ansible-galaxy"
    if ! found_cmd "${galaxy_cmd}"; then
        echo_exit "Command not found: ${galaxy_cmd}"
    fi
    ansible_roles=(
        "andrewrothstein.eksctl,v1.0.41"
        "darkwizard242.azurecli"
        "darkwizard242.gcloudsdk"
        "deekayen.awscli2"
        "ericsysmin.kompose"
        "geerlingguy.docker"
        "geerlingguy.helm"
        "geerlingguy.pip"
    )
    for role in "${ansible_roles[@]}"
    do
        echo_task "Installing ansible role: ${role}"
        "${galaxy_cmd}" install --force "${role}"
    done
}

# DESC: Verifies that ansible is installed and can ping specified inventory
# ARGS: NONE
# OUT:  NONE
verify_ansible () {
	if ! found_file "${ansible_hosts_file}"; then
	    error_exit "File not found: ${ansible_hosts_file}"
	fi
	echo_task "Pinging inventory file: ${ansible_hosts_file}"
	ansible all -m ping
}


# --- Main Function ------------------------------------------------------
main () {
    echo_header "Installing: Software Package Updates..."
    install_updates

    echo_header "Installing: git"
    install_pkg git

    echo_header "Installing: build-essential"
    install_pkg build-essential

    echo_header "Installing: ansible"
    install_pkg ansible

    echo_header "Creating: ansible inventory file"
    create_ansible_inventory

    echo_header "Verifying: ansible installation"
    verify_ansible

    echo_header "Installing: additional ansible roles"
    install_ansible_roles
}

main "$@"
